FROM python:3.7-slim
ENV PYTHONBUFFERED True
WORKDIR /food_delivery_midgard
COPY . .
RUN pip install -r requirements.txt
