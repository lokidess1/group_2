# Правила оформления Merge Request

## 1. Title
### Схема: 
#### 1.1 [Feature/Fix]
#### 1.2 Краткое описание
#### 1.3 Номер таска через решётку - #3
### Пример: [Feature] Added product model #27 
### Еще один: [Fix] Fixed user can't login with gmail account #53
___
## 2. Description 
### Схема:
#### 2.1 Closes #номер_таска или же ISSUE: #номер_таска
#### Например:
#### Closes #53 | ISSUE: #53
___
## 3. Assignee
### Оставляем пока что пустым. Приём реквестов будем осуществлять когда будет два аппрува/лайка
___
## 4. Approval rules
### 4.1 В колонку "No. approval required" ставим цифру 2
___
## 5. Merge Options
### 5.1 Ставим галочку в "Delete source branch... accepted"
___
#### По любым вопросам пишите в чат