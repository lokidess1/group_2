# Food delivery Midgard

### How to install on local machine

1. Clone the project `git clone git@gitlab.com:lokidess1/group_2.git`
2. Go to project folder: `cd group2`
3. Create virtualenv with 'venv' name: `virtualenv venv` (install pip and virtualenv for Windows: https://programwithus.com/learn-to-code/Pip-and-virtualenv-on-Windows/)
4. Activate virtualenv: `source venv/bin/activate` (linux) or `venv\Scripts\activate` (Windows)
5. Install necessary libs: `pip install -r requirements.txt`
6. Install Postgresql: https://www.postgresqltutorial.com/install-postgresql/
7. Open Postgres console and execute following commands:<br />
`CREATE DATABASE midgarddb;`<br />
`CREATE USER midgarddbadmin WITH PASSWORD 'midgard12345';`<br />
`GRANT ALL PRIVILEGES ON DATABASE midgarddb TO midgarddbadmin;`
8. Inside project folder migrate django models: `python manage.py migrate`
9. Collect static: `python manage.py collectstatic`
10. Create superuser: `python manage.py createsuperuser`
11. Run the server: `python manage.py runserver` or run with ssl `python manage.py runsslserver`
12. For login via Google or Facebook open the site on **`https://localhost:8000/`**. Do NOT use `https://127.0.0.1:8000/`

### If you can't start app after `git pull`
1. Install necessary libs: `pip install -r requirements.txt`
2. Migrate models: `python manage.py migrate`
3. Collect static: `python manage.py collectstatic`

### Run app with ssl (Facebook need):
Run the server: `python manage.py runsslserver`

### Git flow and Merge request rules
[Git flow](git_flow.md)<br />
[Merge Request creation flow](MR_creation_flow.md)
