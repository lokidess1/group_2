from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm

from accounts.models import Profile


class SignupForm(UserCreationForm):
    email = forms.EmailField(max_length=255, label='Почта', widget=forms.EmailInput(
        attrs={
            'placeholder': 'name@domen.com'
        }
    ))

    password1 = forms.CharField(
        label='Пароль',
        strip=False,
        widget=forms.PasswordInput,
    )
    password2 = forms.CharField(
        label='Подтверждение пароля',
        widget=forms.PasswordInput,
        strip=False,
    )

    class Meta:
        model = Profile
        fields = ('email',
                  'password1',
                  'password2',
                  'name',
                  'address_delivery',
                  'phone_number')


class LoginUserForm(AuthenticationForm):
    password = forms.CharField(
        label='Пароль',
        strip=False,
        widget=forms.PasswordInput(),
    )

    class Meta:
        fields = ('email', 'password')
