from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.views.generic import UpdateView, CreateView
from accounts.forms import SignupForm, LoginUserForm
from accounts.models import Profile


class SignupView(CreateView):
    template_name = 'signup.html'
    success_url = '/'
    model = Profile
    form_class = SignupForm

    def form_valid(self, form):
        form_valid = super(SignupView, self).form_valid(form)
        email = form.cleaned_data["email"]
        password = form.cleaned_data["password1"]
        auth_user = authenticate(email=email, password=password)
        login(self.request, auth_user)
        return form_valid


class LoginUserView(LoginView):
    template_name = 'login.html'
    success_url = '/'
    form_class = LoginUserForm

    def get_success_url(self):
        return self.success_url


class LogoutUserView(LogoutView):
    next_page = success_url = '/'


class ProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'profile.html'
    login_url = 'login'
    fields = ('email', 'name', 'address_delivery', 'phone_number')
    model = Profile
    success_url = reverse_lazy('profile')

    def get_object(self, queryset=None):
        return self.request.user

