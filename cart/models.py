from django.db import models

from accounts.models import Profile
from product.models import Product


class CartManager(models.Manager):

    def get_cart_items_total_quantity(self, request):
        cart_items = Cart.objects.get(user=request.user).cart_items.all()
        total_quantity = 0
        for cart_item in cart_items:
            total_quantity += cart_item.quantity
        return total_quantity

    def get_cart_items(self, user):
        return self.get().cart_items.all()

    def empty_cart(self, user):
        self.get(user=user).cart_items.all().delete()


class Cart(models.Model):
    user = models.OneToOneField(Profile, null=True, blank=True, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    objects = CartManager()


class CartItem(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    cart = models.ForeignKey('Cart', on_delete=models.CASCADE, related_name='cart_items')
