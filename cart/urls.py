from django.urls import path

from cart.views import CartItemsListView, AddItemView, EmptyCart, GetCartItemsTotalQuantity
from cart.views import CartAdd, CartRemove, CartDetail

app_name = 'cart'

urlpatterns = [
    path('', CartDetail.as_view(), name='cart_detail'),
    path('add/<product_id>/', CartAdd.as_view(), name='cart_add'),
    path('remove/<product_id>/', CartRemove.as_view(), name='cart_remove'),

    path('cart/', CartItemsListView.as_view(), name='cart'),
    path('add-product/', AddItemView.as_view(), name='add_item'),
    #path('cart-items-total-quantity/', GetCartItemsTotalQuantity.as_view(), name='cart-items-quantity'),
    path('empty-cart/', EmptyCart.as_view(), name='empty-cart'),
]
