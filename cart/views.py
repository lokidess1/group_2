from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
from django.views import View
from django.views.generic import ListView, TemplateView

from cart.models import Cart


class CartItemsListView(ListView):
    template_name = 'cart.html'
    model = Cart
    context_object_name = 'cart_items'

    def get_queryset(self):
        return Cart.objects.get_cart_items(self.request.user)


class EmptyCart(View):

    def get(self, request):
        Cart.objects.empty_cart(self.request.user)
        return redirect('cart')


class AddItemView(TemplateView):

    def render_to_response(self, context, **response_kwargs):
        if self.request.user.is_authenticated:
            cart, cart_created = Cart.objects.get_or_create(user=self.request.user)
            product_id = self.request.GET.get('product_id')
            item, created = cart.cart_items.get_or_create(product_id=product_id)
            if not created:
                item.quantity = item.quantity + 1
                item.save()
            data = {'cart_total_items_quantity': Cart.objects.get_cart_items_total_quantity(self.request)}
            return JsonResponse(data)
        else:
            data = {'unauthorized_error': 'true'}
            return JsonResponse(data)


class GetCartItemsTotalQuantity(TemplateView):

    def render_to_response(self, context, **response_kwargs):
        data = {'cart_total_items_quantity': Cart.objects.get_cart_items_total_quantity(self.request)}
        return JsonResponse(data)


class EmptyCart(View):

    def get(self, request):
        Cart.objects.get(user=self.request.user).cart_items.all().delete()
        return redirect('cart')

    # - - - - - - - - - - - - - - - - - - -- -


from django.shortcuts import render, redirect, get_object_or_404

from product.models import Product
from .cart import Cart as Cart2
from .forms import CartAddProductForm


class CartDetail(LoginRequiredMixin, View):
    login_url = 'login'
    # Переделать этот метод для работы с моделью Cart не получилось( много сложных ошибок/ КАЮСЬ!
    def get(self, request):
        cart = Cart2(request)
        for item in cart:
            item['update_quantity_form'] = CartAddProductForm(
                initial={
                    'quantity': item['quantity'],
                    'update': True
                })
        return render(request, 'cart/detail.html', {'cart': cart})


class CartAdd(View):

    def get(self, request, product_id):
        cart = Cart2(request)
        product = get_object_or_404(Product, id=product_id)
        cart.add(product=product,
                 update_quantity=False)
        return redirect('cart:cart_detail')

    def post(self, request, product_id):
        cart = Cart2(request)
        product = get_object_or_404(Product, id=product_id)
        form = CartAddProductForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            cart.add(product=product,
                     quantity=cd['quantity'],
                     update_quantity=cd['update'])
        return redirect('cart:cart_detail')


class CartRemove(View):

    def get(self, request, product_id):
        cart = Cart2(request)
        product = get_object_or_404(Product, id=product_id)
        cart.remove(product)
        return redirect('cart:cart_detail')
