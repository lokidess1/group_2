from .settings import *

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'midgard',
        'USER': 'midgard',
        'PASSWORD': 'midgard',
        'HOST': 'db',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = ['*']
