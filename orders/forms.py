from django import forms
from django.utils.translation import gettext as _
from django.core.exceptions import ValidationError

from .models import Order

PAYMENT_TYPE_CHOICE = {
    ('1', _('Наличные Курьеру')),
    ('2', _('Безналичные')),
    ('3', _('Бонусы'))}


class OrderCreateForm(forms.ModelForm):
    payment_type = forms.ChoiceField(choices=PAYMENT_TYPE_CHOICE, widget=forms.Select())

    class Meta:
        model = Order
        fields = ['address', 'phone', 'comment', 'total_quantity', 'payment_type']
        exclude = ['customer']

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(OrderCreateForm, self).__init__(*args, **kwargs)
        self.fields['total_quantity'].widget.attrs['readonly'] = True

    def clean(self):
        data = super().clean()
        payment_type = data.get('payment_type')
        total_quantity = data.get('total_quantity')

        if payment_type == '3':
            if float(total_quantity) > float(self.request.user.bonuses):
                self.add_error(self.payment_type, "You don't have enough bonuses!")
                raise ValidationError('Sorry! You do not have Bonuses!')
        return data

