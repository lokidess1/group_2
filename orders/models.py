from django.db import models
from product.models import Product
from accounts.models import Profile
from django.urls import reverse
from decimal import Decimal


class Order(models.Model):
    customer = models.ForeignKey(Profile, related_name='orders', on_delete=models.CASCADE)
    address = models.CharField(max_length=250, verbose_name='Адресс доставки')
    phone = models.CharField(max_length=100, verbose_name='Контактный телефон')

    status = models.PositiveIntegerField(default=1, verbose_name='Статус заказа')
    payment_type = models.PositiveIntegerField(default=1, verbose_name='Тип оплаты')
    comment = models.CharField(max_length=150, default='Fast ship plz!', verbose_name='Комментарий к заказу')
    total_quantity = models.DecimalField(max_digits=50, decimal_places=2, default=0, verbose_name='Итоговая цена')

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return f'Order {self.id}'

    def get_total_cost(self):
        return Decimal(sum(item.get_cost() for item in self.items.all()))

    def get_total_quantity(self):
        return sum(item.quantity for item in self.items.all())

    def get_absolute_url(self):
        return reverse('orders:order_detail', args=[self.id])


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='order_items', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f'{self.id}'

    def get_cost(self):
        return self.price * self.quantity
