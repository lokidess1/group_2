from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect, get_object_or_404, HttpResponse
from django.views.generic import View, ListView, DetailView
from decimal import Decimal

from cart.cart import Cart
from .forms import OrderCreateForm
from .models import Order, OrderItem


class OrderCreateView(LoginRequiredMixin, View):
    login_url = 'login'
    model = Order
    template_name = 'orders/order/create.html'

    # TODO need refactor !
    def post(self, request):
        cart = Cart(request)
        form = OrderCreateForm(request.POST, request=request)
        if form.is_valid():
            order = form.save(commit=False)
            order.customer = request.user
            order.save()
            for item in cart:
                OrderItem.objects.create(order=order,
                                         product=item['product'],
                                         price=item['price'],
                                         quantity=item['quantity'])
            cart.clear()
            order.total_quantity = Decimal(order.get_total_cost())
            # TODO Add payment method
            if order.payment_type == 2:
                order.status = 2
            if order.payment_type == 3:
                if order.customer.bonuses >= order.total_quantity:
                    order.customer.bonuses = order.customer.bonuses - Decimal(str(order.total_quantity))
                    order.customer.save()
            bonus = order.total_quantity * Decimal('0.10')
            order.customer.add_bonuses(bonus)
            order.customer.save()
            order.save()
            return redirect('orders:order_list')
        return render(request, self.template_name, context={'form': form, 'cart': cart})

    def get(self, request):
        cart = Cart(request)
        if request.user.is_authenticated:
            form = OrderCreateForm(initial={
                'customer': request.user,
                'address': request.user.address_delivery,
                'phone': request.user.phone_number,
                'payment_type': '1',
                'total_quantity': cart.get_total_price(),
                }
            )
        else:
            form = OrderCreateForm()
        return render(request, 'orders/order/create.html', context={'cart': cart, 'form': form})


class OrderListView(View):
    model = Order
    template_name = 'orders/order/list.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            orders = Order.objects.filter(customer=request.user)
            return render(request, self.template_name, context={'orders': orders})
        else:
            return redirect('login')


class OrderDetailView(DetailView):
    model = Order
    template_name = 'orders/order/detail.html'

    def get(self, request, order_id, *args, **kwargs):
        order = get_object_or_404(Order, id=order_id)
        return render(request, self.template_name, context={'order': order})
