from django.contrib import admin

from product.models import Product, Category, Image


class ImageAdmin(admin.StackedInline):
    model = Image


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    exclude = ('',)
    inlines = [ImageAdmin]


admin.site.register(Category)
admin.site.register(Image)
