from django.db import models
from django.urls import reverse


def product_upload_root(instance, filename):
    return f'product/{instance.name}/{filename}'


def additional_images_upload_root(instance, filename):
    return f'product/{instance.product.name}/additional_images/{filename}'


class Product(models.Model):
    name = models.CharField(max_length=100, verbose_name='Название продукта')
    full_description = models.TextField(blank=True, default='Нет описания', verbose_name='Полное описание')
    short_description = models.CharField(max_length=100, blank=True, default='Нет описания',
                                         verbose_name='Краткое описание')
    main_photo = models.ImageField(upload_to=product_upload_root, verbose_name='Главная картинка')
    proteins = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Белки')
    fats = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Жиры')
    carbohydrates = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Углеводы')
    price = models.DecimalField(max_digits=11, decimal_places=2, verbose_name='Цена за штуку')
    popularity = models.BigIntegerField(default=0, verbose_name='Популярность')
    category = models.ForeignKey('Category', related_name='products', verbose_name='Категории',
                                 on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('product:about', args=[self.id])


class Category(models.Model):
    name = models.CharField(max_length=20, unique=True, verbose_name='Название категории')

    class Meta:
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class Image(models.Model):
    image = models.ImageField(upload_to=additional_images_upload_root, verbose_name='Допольнительные картинки')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='additional_images')
