from django.urls import path

from product.views import MainPageView, ProductListView, ProductDetailView

app_name = 'product'

urlpatterns = (
    path('', MainPageView.as_view(), name='main'),
    path('all/', ProductListView.as_view(), name='products-list'),
    path('product/<int:pk>/about', ProductDetailView.as_view(), name='about'),
)
